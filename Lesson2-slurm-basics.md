# Batch scripts

Create a file `test.sh` with the following contents.

```
#!/bin/sh

printf 'Hello world from %s\n' "$(hostname)"
lscpu > cpu.info
lshw > hw.info
```

Mark it as executable with `chmod +x test.sh` and execute with `./test.sh`.

It should print 

`Hello world from cn84`

and put some information about the CPU in a file called `cpu.info`.

In order to execute this on different machines, we first need to make a decision
on the kind of hardware want to execute this. Run

`sinfo --all --format="%P %l %c %D %m %G"`

to list all partitions (= parts of the network that are bundled together)
under SLURMs administration. You have access to the ones starting with csmpi,
so

`sinfo --all --format="%P %l %c %D %m %G" | grep csmpi`

shows you have access to four partitions. The only difference between the
long and short versions is the time limit. The partitions `csmpi_(short|long)`
have a time limit of respectively 10 minutes and 10 hours, 8 nodes (computers)
with 16 cores and 31 GB of memory each. 
The partitions `csmpi_fpga_(short|long)` have the same time limits, but only
one node, with 32 cores and 128 GB of memory. It also has an NVIDIA A30 GPU.

We have to tell SLURM what partition we want to use.
This is done with `#SBATCH` statements. 

```
#!/bin/sh

#SBATCH --account=csmpistud
#SBATCH --partition=csmpi_fpga_short

printf 'Hello world from %s\n' "$(hostname)"
lscpu > cpu.info
lshw > hw.info
```

We give this script to SLURM using `sbatch test.sh`. Depending on how quick you
are, you can see your job using `squeue --me`, printing something like

```
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
           3150208 csmpi_fpg  test.sh tkoopman PD       0:00      1 (None)
```

ST is the status. This is either PD (pending), which means you are waiting your 
turn, or R (resources) meaning your job is running. You will now have the CPU
information of cn132 in `cpu.info` and `hw.info`. You did not see a 
`Hello world from cn132` though. This is because SLURM redirects your output
to a file called `slurm-<jobid>.out` by default. If you `cat` that, you should
see the output as well as some information about your job. The default name
can make your directory quite messy, so I recommend using 
`#SBATCH --output=<descriptive name>.out`

# Etiquette

SLURM is a fair scheduler, meaning people who submit less jobs will get their 
turn quicker. Nonetheless, every job you submit adds to the load of the 
machine. So if your submitted program is faulty, cancel it with 
`scancel <jobid>` (you can find your jobid with `squeue --me`), or all of your 
jobs with `scancel --me`.
