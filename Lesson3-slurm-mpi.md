The main point of SLURM is to allow multiple users to run MPI jobs without
them interfering with each other. This tutorial will teach you how to use
MPI on a SLURM system.

# mpicc

`mpicc`, short for MPI C Compiler is basically a wrapper around a C compiler,
that also links in the necessary MPI libraries. You use it just the same as
a C compiler. For example, to compile `src/mpi/hello_world.c` from the
parallel programs repository, you can do

`mpicc -Ofast -march=native -mtune=native src/mpi/hello_world -o bin/hello_world_mpi`

to compile it with flags `-Ofast -march=native -mtune=native` and store the
resulting binary in `bin/hello_world`.

# mpirun / mpiexec

`mpirun` and `mpiexec` are exactly the same binary in OpenMPI.
`mpiexec` is in the standard, whereas `mpirun` is not. A pure MPI installation,
such as on your own machine, runs MPI programs with

```
mpirun -n <np> <binary> <arguments>
```

where `<np>` is the number of ranks, `<binary>` is the program name, and
`<arguments>` are the commandline arguments of the binary
(the `argv` passed into `main`).

You can test this out now on your machine with the `hello_world` example in
the [parallel programs repo](https://gitlab.science.ru.nl/parallel-computing2/parallel_programs).

# mpirun / mpiexec on SLURM

On your own computer, one argument `-n` suffices as all tasks have to be
scheduled on one node. On a cluster there are more options. These options
are specified in a batch script, and `mpiexec` is called without options.

```
#!/bin/sh

#SBATCH --account=csmpistud
#SBATCH --partition=csmpi_short
#SBATCH --ntasks=16
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=8
#SBATCH --output=test.out
#SBATCH --time=1:00

BUILD=RELEASE make bin/hello_world_mpi

mpiexec bin/hello_world_mpi
```

Here the `#SBATCH` primitives will tell `mpiexec` what to do. It creates 16
ranks (`ntasks`), using 2 nodes in total, and allocates 8 task to each node.
This is a setup common for pure MPI programs. If the program mixes MPI and
OpenMP, a setup could be

```
#!/bin/sh

#SBATCH --account=csmpistud
#SBATCH --partition=csmpi_short
#SBATCH --ntasks=2
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=8
#SBATCH --output=test.out
#SBATCH --time=1:00

BUILD=RELEASE make bin/hello_world_mixed_mpi

OMP_NUM_THREADS=8 mpiexec --bind-to none bin/hello_world_mixed_mpi
```

Here we have only two ranks total, one per node. With `--cpus-per-task=8` we
make sure every rank has enough cores available for OpenMP. By default, MPI
binds its processes to cores. That is a problem because OpenMP needs more than
one core. For this reason we add the option `--bind-to none`. Without, you will
see that all OpenMP tasks run on CPU 0 or 8 (which is the same physical core,
hyperthreading).

On a production level cluster you should be using `srun` instead of `mpiexec`,
which works the same way as described here. On our teaching cluster we use
the Ubuntu version of OpenMPI, which has not been built to use SLURM,
unfortunately.
