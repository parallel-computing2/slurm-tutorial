# Introduction

For this course you will have access to a shared network of machines.
In order to make sure multiple people can use the system without interfering
with each other, you are only able to login to one machine. From that machine
you tell a program called a *resource manager* what program you want executed,
and on what resources (e.g. number of computers, number of cores, amount of
memory). The resource manager that we use is called SLURM, but let us first
get used to working on a remote system. This tutorial will assume you are
working in a Unix-like environment.

## Science account

To get access to the system you need a science account, which is different from
your RU account. If you are enrolled with the Faculty of Science, you should
have one. You can check by logging into [dhz](https://dhz.science.ru.nl/login)
(via RADBOUD ACCOUNT), and then it will list your username if you have one.
If not, contact a teacher.

## SSH

In order to access the network, you will have to be connected to the
university's network. If you are working from home, you will have to
use a [VPN](https://cncz.science.ru.nl/nl/howto/vpn/).

Having done that, open up a terminal and execute the command

`ssh tkoopman@cnlogin22.science.ru.nl`

where you replace tkoopman by your science login. You will be asked to enter
your password, and then you should see a screen like this.

    Welcome to Ubuntu 22.04.2 LTS (GNU/Linux 5.19.0-46-generic x86_64)
    
                 ___  _  _
      ___ _ __  ( _ )| || |
     / __| '_ \ / _ \| || |_
    | (__| | | | (_) |__   _|
     \___|_| |_|\___/   |_|(_)science.ru.nl
    
    
    cn84 will reboot every first Monday of the month
    
    
     ~ this system is managed by C&CZ ~
     11:15:25 up 7 days, 17:23,  8 users,  load average: 5.20, 5.41, 6.55
    
    Last login: Wed Jan 17 11:12:46 2024 from ip-145-137-143-111.wlan-int.ru.nl

### SCP

Copy-pasting is a huge pain between systems, so the best way to transfer files
from your machine to a remote one, is to use `scp`. This program works just like
`cp`, but you can use it between machines. To access a file on cn84, simply
prepend `tkoopman@cnlogin22.science.ru.nl:` to the path (again, replace tkoopman
by your science login). So in order to copy over your bash configuration file,
do (on your machine)

```
scp ~/.bashrc tkoopman@cnlogin22.science.ru.nl:~
```

You can read this as follows

```
scp <source file> <machine>:<destination directory>
```

So this copies `~/.bashrc` from your machine, to the remote, to directory `~`.

If you want to copy a file from the remote to your machine's working directory,
e.g. `mandelbrot.pgm` in `~/parallel_programs` the command would become

```
scp tkoopman@cnlogin22.science.ru.nl:~/parallel_programs/mandelbrot.pgm .
```

Note that `.` is important here, it is your current directory. Equivalently

```
scp tkoopman@cnlogin22.science.ru.nl:~/parallel_programs/mandelbrot.pgm $(pwd)
```

This has to be executed through a terminal on your machine, not the remote
machine.

### Logging in via keys

You are probably already annoyed by having to type out the full machine address
and your password. We first create an alias for the machine. Create the file
`~/.ssh/config` and add

```
Host cncz
    HostName cnlogin22.science.ru.nl
    user tkoopman
```

You can now login by typing `ssh cncz` and entering your password. To get rid
of password authentication, we have to copy over your public ssh key
([generate one if you don't have any](https://git-scm.com/book/en/v2/Git-on-the-Server-Generating-Your-SSH-Public-Key)):

`scp ~/.ssh/id_rsa.pub cncz:~/.ssh/authorized_keys`

You can now use `scp` and `ssh` without entering your password.

# Etiquette

The login node is a machine shared by many people, with limited compute. So
please do not run heavy programs such as compiles longer than a few seconds,
or VScode. The machine has vi, emacs, and nano. You can also program on your own
machine and `scp` the files over.
